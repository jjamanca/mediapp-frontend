import { Component, OnInit, ViewChild } from '@angular/core';
import { PacienteService } from 'src/app/_service/paciente.service';
import { Paciente } from 'src/app/_model/paciente';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-paciente',
  templateUrl: './paciente.component.html',
  styleUrls: ['./paciente.component.css']
})
export class PacienteComponent implements OnInit {
  // pacientes : Paciente[];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource: MatTableDataSource<Paciente>;
  displayedColumns: string[] = ['idPaciente', 'nombres', 'apellidos', 'acciones'];
  cantidad : number = 0;
  constructor(
    private pacienteService: PacienteService,
    private snackBar  : MatSnackBar
    ) { }

  ngOnInit(): void {
    // this.pacienteService.listar().subscribe(data=>{
    //   this.crearTabla(data);
    // });
    this.pacienteService.listarPageable(0, 10).subscribe(data=>{
      // console.log(data);
      // this.crearTabla(data);
      this.dataSource = new MatTableDataSource(data.content);
      this.cantidad = data.totalElements;
      this.dataSource.sort=this.sort;
    });
    this.pacienteService.getPacienteCambio().subscribe(data=>{
      this.crearTabla(data);
    });
    this.pacienteService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'Aviso', {
        duration: 2000,
        verticalPosition: 'top',
        horizontalPosition: 'right'
      });
    });
  }
  eliminar(id : number) {
    this.pacienteService.eliminar(id).subscribe(() => {
      this.pacienteService.listar().subscribe((data) => {
        this.pacienteService.setPacienteCambio(data);
        // this.crearTabla(data);// es lo mismo que la fila de arriba
        this.pacienteService.setMensajeCambio('Se eliminó');
      });
    });

  }
  crearTabla(data : Paciente[]) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
  filtrar(valor: string) {
    this.dataSource.filter = valor.trim().toLowerCase();
  } 
  mostrarMas(e: any) {
    // console.log(e);
    this.pacienteService.listarPageable(e.pageIndex, e.pageSize).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort = this.sort;
    });
  }
}
//min 28:20 Spring Pageable, Plantillas Themeforest, MenuMultiLevel
