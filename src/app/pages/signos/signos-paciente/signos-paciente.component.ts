import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Paciente } from 'src/app/_model/paciente';
import { PacienteService } from 'src/app/_service/paciente.service';

@Component({
  selector: 'app-signos-paciente',
  templateUrl: './signos-paciente.component.html',
  styleUrls: ['./signos-paciente.component.css']
})
export class SignosPacienteComponent implements OnInit {

  id: number;
  form :FormGroup;
  edicion: boolean;
  formIngreso : FormGroup;
  constructor(
    private dialogRef : MatDialogRef<SignosPacienteComponent>, 
    private route : ActivatedRoute,
    private router : Router,
    private pacienteService : PacienteService,
    @Inject(MAT_DIALOG_DATA) private data : Paciente
  ) { }

  ngOnInit(): void {
    // this.formIngreso = {...this.data};
    this.form = new FormGroup({
      'id' :  new FormControl(0),
      'nombres' :  new FormControl(''),
      'apellidos' :  new FormControl(''),
      'dni' :  new FormControl(''),
      'telefono' :  new FormControl(''),
      'direccion' :  new FormControl(''),
      'email' :  new FormControl('')
    });
    this.route.params.subscribe((data : Params) => {
      this.id = data['id'];
      this.edicion = data['id'] != null;
      
      // this.initForm();
    });
  }
  
  operar(): void {
    let paciente = new Paciente();
    paciente.idPaciente = this.form.value['id'];
    paciente.nombres = this.form.value['nombres'];
    paciente.apellidos = this.form.value['apellidos'];
    paciente.dni = this.form.value['dni'];
    paciente.telefono = this.form.value['telefono'];
    paciente.direccion = this.form.value['direccion'];
    paciente.email = this.form.value['email'];

      this.pacienteService.registrar(paciente).subscribe(() => {
        // this.pacienteService.listar().subscribe(data => {
        //   this.pacienteService.setPacienteCambio(data);
        //   this.pacienteService.setMensajeCambio('Se registro');
        // });
        this.data = paciente;
      });
    
      this.dialogRef.close();
  }
}
