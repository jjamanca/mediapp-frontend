import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { switchMap } from 'rxjs/operators';
import { Signos } from 'src/app/_model/signos';
import { SignosService } from 'src/app/_service/signos.service';

@Component({
  selector: 'app-signos-delete',
  templateUrl: './signos-delete.component.html',
  styleUrls: ['./signos-delete.component.css']
})
export class SignosDeleteComponent implements OnInit {
   signos : Signos
  constructor(
    private dialogRef : MatDialogRef<SignosDeleteComponent>, 
    @Inject(MAT_DIALOG_DATA) private data : Signos,
    private signosService : SignosService
  ) { }

  ngOnInit(): void {
    this.signos = {...this.data};
  }

  eliminar() {
    this.signosService.eliminar(this.signos.idSignos).pipe(switchMap(()=> {
       return this.signosService.listar();
    }))
    .subscribe(data => {
      this.signosService.setMensajeCambio("SE ELIMINO");
      this.signosService.setSignosCambio(data);
    });
    this.dialogRef.close();
  }
  cerrar() {
    this.dialogRef.close();
  }
}
