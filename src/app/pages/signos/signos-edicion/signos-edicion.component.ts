import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { Paciente } from 'src/app/_model/paciente';
import { Signos } from 'src/app/_model/signos';
import { PacienteService } from 'src/app/_service/paciente.service';
import { SignosService } from 'src/app/_service/signos.service';
import { SignosPacienteComponent } from '../signos-paciente/signos-paciente.component';

@Component({
  selector: 'app-signos-edicion',
  templateUrl: './signos-edicion.component.html',
  styleUrls: ['./signos-edicion.component.css']
})
export class SignosEdicionComponent implements OnInit {
  
  form :FormGroup;
  id: number;

  fechaSeleccionada : Date = new Date();
  maxFecha : Date = new Date();  
  edicion: boolean;
  signos : Signos;
  pacientes : Paciente[];
  pacienteSeleccionado : Paciente;
  myControlPaciente : FormControl = new FormControl();
  pacientesFiltrados$ : Observable<Paciente[]>;
  constructor(
    private route : ActivatedRoute,
    private router : Router,
    private signosService : SignosService,
    private pacienteService : PacienteService,
    private dialog: MatDialog

  ) { }

  ngOnInit(): void {
    this.signos = new Signos();
    this.form = new FormGroup({
      'id' : new FormControl(0),
      // 'paciente' : new FormControl(''),
      'paciente' : this.myControlPaciente,
      'fecha' : new FormControl(''),
      'pulso' : new FormControl(''),
      'respiratorio' : new FormControl(''),
      'temperatura' : new FormControl('')
    });
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id']!=null;
      this.initForm();
    });
    this.listarPacientes();
    this.pacientesFiltrados$ = this.myControlPaciente.valueChanges.pipe(map(val => this.filtrarPacientes(val)));
  }
  initForm() {
    if(this.edicion) {
      this.signosService.listarPorId(this.id).subscribe(data => {
        let id = data.idSignos;
        let paciente = data.paciente;
        let fecha = data.fecha;
        let pulso = data.pulso;
        let respiratorio = data.respiratorio;
        let temperatura = data.temperatura;
        this.form = new FormGroup({
          'id' : new FormControl(id),
          'paciente' : new FormControl(paciente),
          'fecha' : new FormControl(fecha),
          'pulso' : new FormControl(pulso),
          'respiratorio' : new FormControl(respiratorio),
          'temperatura' : new FormControl(temperatura)
        });
      });
    }
  }
  operar() {
    
    this.signos.idSignos = this.form.value['id'];
    this.signos.fecha = this.form.value['fecha'];
    this.signos.pulso = this.form.value['pulso'];
    this.signos.temperatura = this.form.value['temperatura'];
    this.signos.respiratorio = this.form.value['respiratorio'];
    this.signos.paciente = this.form.value['paciente'];

    // this.especialidad.idEspecialidad = this.form.value['id'];
    // this.especialidad.nombre = this.form.value['nombre'];
    // this.especialidad.descripcion = this.form.value['descripcion'];

    if(this.signos != null && this.signos.idSignos > 0) {
      this.signosService.modificar(this.signos).pipe(switchMap(() => {
        return this.signosService.listar();
      })).subscribe(data => {
        this.signosService.setSignosCambio(data);
        this.signosService.setMensajeCambio('SE MODIFICO');
      });
    } else {
      this.signosService.registrar(this.signos).subscribe(data => {
        this.signosService.listar().subscribe(signos => {
          this.signosService.setSignosCambio(signos);
          this.signosService.setMensajeCambio('SE REGISTRO');
        })
      });
    }
    this.router.navigate(['pages/signos']);
  }

  mostrarPaciente(val : Paciente) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }
  filtrarPacientes(val : any) {
    if(val != null && val.idPaciente > 0) {
      return this.pacientes.filter(el =>
        el.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || el.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || el.dni.includes(val)
      );
    }
    return this.pacientes.filter(el => 
        el.nombres.toLowerCase().includes(val?.toLowerCase()) || el.apellidos.toLowerCase().includes(val?.toLowerCase()) || el.dni.includes(val)
    );
  }
  listarPacientes() {
    this.pacienteService.listar().subscribe(data => {
      this.pacientes=data;
    });
  }
  // nuevoPaciente() {
  //   this.dialog.open(SignosPacienteComponent, {
  //     width: '250px',
  //     data: this.form.value['paciente']
  //   });
  // }
}
