import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { switchMap } from 'rxjs/operators';
import { Medico } from 'src/app/_model/medico';
import { MedicoService } from 'src/app/_service/medico.service';

@Component({
  selector: 'app-confirm-dialogo',
  templateUrl: './confirm-dialogo.component.html',
  styleUrls: ['./confirm-dialogo.component.css']
})
export class ConfirmDialogoComponent implements OnInit {

  medico : Medico
  constructor(
    private dialogRef : MatDialogRef<ConfirmDialogoComponent>, 
    @Inject(MAT_DIALOG_DATA) private data : Medico,
    private medicoService : MedicoService
  ) { }

  ngOnInit(): void {
    this.medico = {...this.data};
  }
  eliminar() {
    this.medicoService.eliminar(this.medico.idMedico).pipe(switchMap(()=> {
       return this.medicoService.listar();
    }))
    .subscribe(data => {
      this.medicoService.setMensajeCambio("SE ELIMINO");
      this.medicoService.setMedicoCambio(data);
    });
    this.dialogRef.close();
  }
  cerrar() {
    this.dialogRef.close();
  }
}
