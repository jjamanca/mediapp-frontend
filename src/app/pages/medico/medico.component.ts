import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { switchMap } from 'rxjs/operators';
import { Medico } from 'src/app/_model/medico';
import { MedicoService } from 'src/app/_service/medico.service';
import { ConfirmDialogoComponent } from './confirm-dialogo/confirm-dialogo.component';
import { MedicoDialogoComponent } from './medico-dialogo/medico-dialogo.component';

@Component({
  selector: 'app-medico',
  templateUrl: './medico.component.html',
  styleUrls: ['./medico.component.css']
})
export class MedicoComponent implements OnInit {

  displayedColumns: string[] = ['idmedico', 'nombres', 'apellidos', 'cmp', 'acciones'];
  dataSource: MatTableDataSource<Medico>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private medicoService : MedicoService,
    private dialog: MatDialog,
    private snackBar  : MatSnackBar
  ) { }

  ngOnInit(): void {
    this.medicoService.listar().subscribe(data => {
      this.crearTabla(data);
    });
    this.medicoService.getMedicoCambio().subscribe(data=>{
      this.crearTabla(data);
    });
    this.medicoService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'Aviso', {
        duration: 2000,
        verticalPosition: 'top',
        horizontalPosition: 'right'
      });
    });
  }
  //59:35
  crearTabla(data : Medico[]) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
  filtrar(valor: string) {
    this.dataSource.filter = valor.trim().toLowerCase();
  } 
  eliminar(medico : Medico) {
    // this.medicoService.eliminar(medico.idMedico).pipe(switchMap(()=> {
    //   // return this.medicoService.listar();
    // }))
    // .subscribe(data => {
    //   // this.crearTabla(data);
    //   this.medicoService.setMensajeCambio("SE ELIMINO");
    //   this.medicoService.setMedicoCambio(data);
    // });
    this.dialog.open(ConfirmDialogoComponent, {
      width: '250px',
      data: medico
    });
  }
  abrirDialogo(medico?: Medico) {
    this.dialog.open(MedicoDialogoComponent, {
      width: '250px',
      data: medico
    });
  }

}
