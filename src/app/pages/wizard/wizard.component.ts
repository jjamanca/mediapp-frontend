import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatStepper } from '@angular/material/stepper';
import * as moment from 'moment';
import { ConsultaListaExamenDTO } from 'src/app/_dto/consultaListaExamenDTO';
import { Consulta } from 'src/app/_model/consulta';
import { DetalleConsulta } from 'src/app/_model/detalleConsulta';
import { Especialidad } from 'src/app/_model/especialidad';
import { Examen } from 'src/app/_model/examen';
import { Medico } from 'src/app/_model/medico';
import { Paciente } from 'src/app/_model/paciente';
import { ConsultaService } from 'src/app/_service/consulta.service';
import { EspecialidadService } from 'src/app/_service/especialidad.service';
import { ExamenService } from 'src/app/_service/examen.service';
import { MedicoService } from 'src/app/_service/medico.service';
import { PacienteService } from 'src/app/_service/paciente.service';

@Component({
  selector: 'app-wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.css']
})
export class WizardComponent implements OnInit {
  @ViewChild('stepper') stepper: MatStepper;
  isLinear : boolean = false;
  primerFormGroup : FormGroup;
  segundoFormGroup : FormGroup;

  pacientes : Paciente[];
  medicos : Medico[];
  especialidades : Especialidad[];
  examenes : Examen[];

  pacienteSeleccionado : Paciente;
  medicoSeleccionado : Medico;
  especialidadSeleccionada : Especialidad;
  examenSeleccionado : Examen;

  fechaSeleccionada : Date = new Date();
  maxFecha : Date = new Date();

  diagnostico : string;
  tratamiento : string;
  mensaje : string;

  detalleConsulta : DetalleConsulta[] = [];
  examenesSeleccionados : Examen[] = [];
  
  consultorios : number[] = [];
  consultorioSeleccionado : number = 0; 

  constructor(
    private fromBuilder : FormBuilder,
    private pacienteService : PacienteService,
    private medicoService : MedicoService,
    private examenSerice : ExamenService,
    private especialidadService : EspecialidadService,
    private consultaService : ConsultaService,
    private snackBar : MatSnackBar
  ) { }

  ngOnInit(): void {
    //44:20
    this.primerFormGroup = this.fromBuilder.group({
      cboPaciente : [new FormControl(), Validators.required],
      fecha : [new FormControl(), new FormControl(new Date(), [Validators.required])],
      'diagnostico' : new FormControl(''),
      'tratamiento' : new FormControl('')
    });
    this.segundoFormGroup = this.fromBuilder.group({
      // hidden : ['', Validators.required]
    });
    this.listarEspecialidades();
    this.listarExamenes();
    this.listarMedicos();
    this.listarPacientes();
    this.listarConsultorios();
  }
  listarConsultorios() {
    for (let i = 1; i <= 20; i++) {
      this.consultorios.push(i);
    }
  }
  seleccionarConsultorio(c : number) {
    this.consultorioSeleccionado = c;
  }
  seleccionarPaciente(e : any) {
    this.pacienteSeleccionado = e.value;
  }
  seleccionarEspecialidad(e : any) {
    this.especialidadSeleccionada = e.value;
  }
  listarEspecialidades() {
    this.especialidadService.listar().subscribe(data => {
      this.especialidades = data;
    })
  }
  listarMedicos() {
    this.medicoService.listar().subscribe(data => {
      this.medicos=data;
    });
  }
  listarExamenes() {
    this.examenSerice.listar().subscribe(data => {
      this.examenes = data;
    });
  }
  listarPacientes() {
    this.pacienteService.listar().subscribe(data => {
      this.pacientes = data;
    });
  }
  agregar() {
    if(this.diagnostico != null && this.tratamiento != null) {
      let det = new DetalleConsulta();
      det.diagnostico = this.diagnostico;
      det.tratamiento = this.tratamiento;
      this.detalleConsulta.push(det);
      this.diagnostico = null;
      this.tratamiento =  null;
    } else {
      this.mensaje = `Debe agregar un diagnostico y tratamiento`;
      this.snackBar.open(this.mensaje, 'Aviso', {duration: 2000});
    }
  }
  removerDiagnostico(index : number) {
    this.detalleConsulta.splice(index, 1);
  }
  agregarExamen() {
    if(this.examenSeleccionado) {
      let cont = 0;
      for (let i = 0; i < this.examenesSeleccionados.length; i++) {
        let examen = this.examenesSeleccionados[i];
        if(this.examenSeleccionado.idExamen === examen.idExamen) {
          cont++;
          return;
        }
      }
      if(cont > 0) {
        this.mensaje= `El examen se encuentra en la lista`;
        this.snackBar.open(this.mensaje, 'AVISO', {duration: 2000});
      } else {
        this.examenesSeleccionados.push(this.examenSeleccionado);
      }
    } else {
      this.mensaje= `Dede agregar un examen`;
      this.snackBar.open(this.mensaje, 'AVISO', {duration: 2000});
    }
  }
  removerExamen(index : number) {
    this.examenesSeleccionados.splice(index, 1);
  }
  seleccionarMedico(medico : Medico) {
    this.medicoSeleccionado = medico;
  }
  nextManualStep() {
    if (this.consultorioSeleccionado > 0) {
      this.stepper.linear = false;
      this.stepper.next();
    } else {
      this.snackBar.open('Debes seleccionar un consultorio', 'INFO', { duration: 2000 });
    }
  }
  registrar() {
    let consulta = new Consulta();
    consulta.especialidad = this.especialidadSeleccionada;
    consulta.medico = this.medicoSeleccionado;
    consulta.paciente = this.pacienteSeleccionado;
    consulta.fecha = moment(this.fechaSeleccionada).format('YYYY-MM-DDTHH:mm:ss');
    consulta.detalleConsulta = this.detalleConsulta;
    consulta.numConsultorio = `C${this.consultorioSeleccionado}`;

    let consultaListaExamenDTO = new ConsultaListaExamenDTO();
    consultaListaExamenDTO.consulta = consulta;
    consultaListaExamenDTO.lstExamen = this.examenesSeleccionados;

    this.consultaService.registrarTransaccion(consultaListaExamenDTO).subscribe(() => {
      this.snackBar.open("Se registró", "Aviso", { duration: 2000 });

      setTimeout(() => {
        this.limpiarControles();
      }, 2000);
    });
  }

  limpiarControles() {
    this.detalleConsulta = [];
    this.examenesSeleccionados = [];
    this.diagnostico = '';
    this.tratamiento = '';
    this.pacienteSeleccionado = undefined;
    this.especialidadSeleccionada = undefined;
    this.medicoSeleccionado = undefined;
    this.examenSeleccionado = undefined;
    this.fechaSeleccionada = new Date();
    this.fechaSeleccionada.setHours(0);
    this.fechaSeleccionada.setMinutes(0);
    this.fechaSeleccionada.setSeconds(0);
    this.fechaSeleccionada.setMilliseconds(0);
    this.consultorioSeleccionado = 0;
    this.mensaje = '';
    this.stepper.reset();
  }
  estadoBotonRegistrar() {
    return (this.detalleConsulta.length === 0 || this.especialidadSeleccionada === undefined || this.medicoSeleccionado === undefined || this.pacienteSeleccionado === undefined || this.consultorioSeleccionado === 0);
  }
}
