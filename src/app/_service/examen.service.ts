import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Examen } from '../_model/examen';
import { GenericService } from './generic.service';

@Injectable({
  providedIn: 'root'
})
export class ExamenService extends GenericService<Examen> {
  private examenCambio : Subject<Examen[]> = new Subject<Examen[]>();
  private mensajeCambio : Subject<string> = new Subject<string>();
  constructor(protected http : HttpClient) {
    super(
      http, `${environment.HOST}/examenes`
    ) 
  }
  getExamenCambio() {
    return this.examenCambio.asObservable();
  }
  setExamenCambio(lista : Examen[]) {
    this.examenCambio.next(lista);
  }
  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }
  setMensajeCambio(mensaje : string) {
    this.mensajeCambio.next(mensaje);
  }
}
